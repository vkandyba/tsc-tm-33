package ru.vkandyba.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Session;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.system.AccessDeniedException;
import ru.vkandyba.tm.util.TerminalUtil;

public class AuthChangePasswordCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "change-password";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change password...";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("Enter new password");
        @Nullable final String password = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().changeUserPassword(session, password);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
