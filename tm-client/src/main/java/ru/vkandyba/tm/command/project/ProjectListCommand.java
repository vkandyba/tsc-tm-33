package ru.vkandyba.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.command.AbstractCommand;
import ru.vkandyba.tm.endpoint.Project;
import ru.vkandyba.tm.endpoint.Session;

import java.util.List;

public class ProjectListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "project-list";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show project list...";
    }

    @Override
    public void execute() {
        @NotNull final Session session = serviceLocator.getSession();
        System.out.println("[LIST PROJECTS]");
        List<Project> projects = serviceLocator.getProjectEndpoint().findAllProjects(session);
        for (Project project : projects) {
            System.out.println(projects.indexOf(project) + 1 + ". " + project.getName() + " " + project.getId() + ": " + project.getDescription());
        }
        System.out.println("[OK]");
    }

}
