package ru.vkandyba.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-09-01T19:08:07.931+03:00
 * Generated source version: 3.4.2
 *
 */
@WebService(targetNamespace = "http://endpoint.tm.vkandyba.ru/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/finishProjectByNameRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/finishProjectByNameResponse")
    @RequestWrapper(localName = "finishProjectByName", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.FinishProjectByName")
    @ResponseWrapper(localName = "finishProjectByNameResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.FinishProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project finishProjectByName(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/removeProjectWithTasksByIdRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/removeProjectWithTasksByIdResponse")
    @RequestWrapper(localName = "removeProjectWithTasksById", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.RemoveProjectWithTasksById")
    @ResponseWrapper(localName = "removeProjectWithTasksByIdResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.RemoveProjectWithTasksByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project removeProjectWithTasksById(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/removeProjectByIndexRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/removeProjectByIndexResponse")
    @RequestWrapper(localName = "removeProjectByIndex", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.RemoveProjectByIndex")
    @ResponseWrapper(localName = "removeProjectByIndexResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.RemoveProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project removeProjectByIndex(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.Integer arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/showProjectByIdRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/showProjectByIdResponse")
    @RequestWrapper(localName = "showProjectById", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ShowProjectById")
    @ResponseWrapper(localName = "showProjectByIdResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ShowProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project showProjectById(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/updateProjectByIndexRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/updateProjectByIndexResponse")
    @RequestWrapper(localName = "updateProjectByIndex", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.UpdateProjectByIndex")
    @ResponseWrapper(localName = "updateProjectByIndexResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.UpdateProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project updateProjectByIndex(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.Integer arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        java.lang.String arg2,
        @WebParam(name = "arg3", targetNamespace = "")
        java.lang.String arg3
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/updateProjectByIdRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/updateProjectByIdResponse")
    @RequestWrapper(localName = "updateProjectById", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.UpdateProjectById")
    @ResponseWrapper(localName = "updateProjectByIdResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.UpdateProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project updateProjectById(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        java.lang.String arg2,
        @WebParam(name = "arg3", targetNamespace = "")
        java.lang.String arg3
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/startProjectByIndexRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/startProjectByIndexResponse")
    @RequestWrapper(localName = "startProjectByIndex", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.StartProjectByIndex")
    @ResponseWrapper(localName = "startProjectByIndexResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.StartProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project startProjectByIndex(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.Integer arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/changeProjectStatusByIndexRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/changeProjectStatusByIndexResponse")
    @RequestWrapper(localName = "changeProjectStatusByIndex", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ChangeProjectStatusByIndex")
    @ResponseWrapper(localName = "changeProjectStatusByIndexResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ChangeProjectStatusByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project changeProjectStatusByIndex(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.Integer arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Status arg2
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/removeProjectByNameRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/removeProjectByNameResponse")
    @RequestWrapper(localName = "removeProjectByName", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.RemoveProjectByName")
    @ResponseWrapper(localName = "removeProjectByNameResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.RemoveProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project removeProjectByName(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/showProjectByIndexRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/showProjectByIndexResponse")
    @RequestWrapper(localName = "showProjectByIndex", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ShowProjectByIndex")
    @ResponseWrapper(localName = "showProjectByIndexResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ShowProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project showProjectByIndex(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.Integer arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/addProjectRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/addProjectResponse")
    @RequestWrapper(localName = "addProject", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.AddProject")
    @ResponseWrapper(localName = "addProjectResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.AddProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project addProject(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Project arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/startProjectByNameRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/startProjectByNameResponse")
    @RequestWrapper(localName = "startProjectByName", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.StartProjectByName")
    @ResponseWrapper(localName = "startProjectByNameResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.StartProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project startProjectByName(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/findAllProjectsRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/findAllProjectsResponse")
    @RequestWrapper(localName = "findAllProjects", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.FindAllProjects")
    @ResponseWrapper(localName = "findAllProjectsResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.FindAllProjectsResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.vkandyba.tm.endpoint.Project> findAllProjects(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/startProjectByIdRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/startProjectByIdResponse")
    @RequestWrapper(localName = "startProjectById", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.StartProjectById")
    @ResponseWrapper(localName = "startProjectByIdResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.StartProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project startProjectById(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/removeProjectByIdRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/removeProjectByIdResponse")
    @RequestWrapper(localName = "removeProjectById", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.RemoveProjectById")
    @ResponseWrapper(localName = "removeProjectByIdResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.RemoveProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project removeProjectById(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/changeProjectStatusByIdRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/changeProjectStatusByIdResponse")
    @RequestWrapper(localName = "changeProjectStatusById", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ChangeProjectStatusById")
    @ResponseWrapper(localName = "changeProjectStatusByIdResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ChangeProjectStatusByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project changeProjectStatusById(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Status arg2
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/changeProjectStatusByNameRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/changeProjectStatusByNameResponse")
    @RequestWrapper(localName = "changeProjectStatusByName", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ChangeProjectStatusByName")
    @ResponseWrapper(localName = "changeProjectStatusByNameResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ChangeProjectStatusByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project changeProjectStatusByName(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Status arg2
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/finishProjectByIdRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/finishProjectByIdResponse")
    @RequestWrapper(localName = "finishProjectById", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.FinishProjectById")
    @ResponseWrapper(localName = "finishProjectByIdResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.FinishProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project finishProjectById(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.String arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/finishProjectByIndexRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/finishProjectByIndexResponse")
    @RequestWrapper(localName = "finishProjectByIndex", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.FinishProjectByIndex")
    @ResponseWrapper(localName = "finishProjectByIndexResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.FinishProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.vkandyba.tm.endpoint.Project finishProjectByIndex(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        java.lang.Integer arg1
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/clearProjectsRequest", output = "http://endpoint.tm.vkandyba.ru/ProjectEndpoint/clearProjectsResponse")
    @RequestWrapper(localName = "clearProjects", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ClearProjects")
    @ResponseWrapper(localName = "clearProjectsResponse", targetNamespace = "http://endpoint.tm.vkandyba.ru/", className = "ru.vkandyba.tm.endpoint.ClearProjectsResponse")
    public void clearProjects(

        @WebParam(name = "arg0", targetNamespace = "")
        ru.vkandyba.tm.endpoint.Session arg0
    );
}
