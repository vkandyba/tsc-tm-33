package ru.vkandyba.tm.model;

import org.jetbrains.annotations.Nullable;

public class Session extends AbstractEntity implements Cloneable{

    public Session() {
    }

    @Override
    public Session clone() throws CloneNotSupportedException {
        return (Session) super.clone();
    }

    @Nullable
    private Long timestamp;

    @Nullable
    private String userId;

    @Nullable
    private String signature;

    @Nullable
    public Long getTimestamp() {
        return timestamp;
    }

    @Nullable
    public String getUserId() {
        return userId;
    }

    @Nullable
    public String getSignature() {
        return signature;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}
