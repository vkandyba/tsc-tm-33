package ru.vkandyba.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.endpoint.ITaskEndpoint;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.model.Session;

import javax.jws.WebService;
import java.util.List;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Nullable
    @Override
    public List<Task> findAllTasks(@NotNull Session session) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findAll(session.getUserId());
    }

    @Override
    public @Nullable Task addTask(@NotNull Session session, @NotNull Task task) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().add(session.getUserId(),task);
    }

    @Override
    public @Nullable Task changeTaskStatusById(@NotNull Session session, @NotNull String taskId, @NotNull Status status) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusById(session.getUserId(), taskId, status);
    }

    @Override
    public @Nullable Task changeTaskStatusByIndex(@NotNull Session session, @NotNull Integer index, @NotNull Status status) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusByIndex(session.getUserId(), index, status);
    }

    @Override
    public @Nullable Task changeTaskStatusByName(@NotNull Session session, @NotNull String name, @NotNull Status status) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().changeStatusByName(session.getUserId(), name, status);
    }

    @Override
    public @Nullable Task finishTaskById(@NotNull Session session, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishById(session.getUserId(), taskId);
    }

    @Override
    public @Nullable Task finishTaskByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishByIndex(session.getUserId(), index);
    }

    @Override
    public @Nullable Task finishTaskByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().finishByName(session.getUserId(), name);
    }

    @Override
    public @Nullable Task removeTaskById(@NotNull Session session, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeById(session.getUserId(), taskId);
    }

    @Override
    public @Nullable Task removeTaskByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeByIndex(session.getUserId(), index);
    }

    @Override
    public @Nullable Task removeTaskByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeByName(session.getUserId(), name);
    }

    @Override
    public @Nullable Task showTaskById(@NotNull Session session, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findById(session.getUserId(), taskId);
    }

    @Override
    public @Nullable Task showTaskByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findByIndex(session.getUserId(), index);
    }

    @Override
    public @Nullable Task startTaskById(@NotNull Session session, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startById(session.getUserId(), taskId);
    }

    @Override
    public @Nullable Task startTaskByIndex(@NotNull Session session, @NotNull Integer index) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startByIndex(session.getUserId(), index);
    }

    @Override
    public @Nullable Task startTaskByName(@NotNull Session session, @NotNull String name) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().startByName(session.getUserId(), name);
    }

    @Override
    public @Nullable Task updateTaskByIndex(@NotNull Session session, @NotNull Integer index, @NotNull String name, @NotNull String description) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateByIndex(session.getUserId(), index, name, description);
    }

    @Override
    public @Nullable Task updateTaskById(@NotNull Session session, @NotNull String taskId, @NotNull String name, @NotNull String description) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().updateById(session.getUserId(), taskId, name, description);
    }

    @Override
    public @Nullable Task removeTaskWithTasksById(@NotNull Session session, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().removeById(session.getUserId(), taskId);
    }

    @Override
    public void clearTasks(@NotNull Session session) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().clear(session.getUserId());
    }

    @Override
    public @Nullable Task unbindTaskFromProjectById(@NotNull Session session, @NotNull String projectId, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().unbindTaskToProjectById(session.getUserId(), projectId, taskId);
    }

    @Override
    public @Nullable List<Task> showAllTasksByProjectId(@NotNull Session session, @NotNull String projectId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findAllTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    public @Nullable Task bindTaskToProjectById(@NotNull Session session, @NotNull String projectId, @NotNull String taskId) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().bindTaskToProjectById(session.getUserId(), projectId, taskId);
    }

}
