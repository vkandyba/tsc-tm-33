package ru.vkandyba.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.ISessionRepository;
import ru.vkandyba.tm.api.service.IPropertyService;
import ru.vkandyba.tm.api.service.ISessionService;
import ru.vkandyba.tm.api.service.ServiceLocator;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.exception.system.AccessDeniedException;
import ru.vkandyba.tm.model.Session;
import ru.vkandyba.tm.model.User;
import ru.vkandyba.tm.util.HashUtil;

import java.util.Optional;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    @NotNull
    private final ISessionRepository sessionRepository;

    public SessionService(
            @NotNull final ServiceLocator serviceLocator,
            @NotNull final ISessionRepository sessionRepository
    ) {
        super(sessionRepository);
        this.serviceLocator = serviceLocator;
        this.sessionRepository = sessionRepository;
    }

    @Nullable
    @Override
    public Session open(
            @Nullable final String login,
            @Nullable final String password
    ) {
        final boolean check = checkDataAccess(login, password);
        if (!check) return null;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return null;
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }


    @Override
    @Nullable
    public Session close(@Nullable final Session session) {
        sessionRepository.removeById(session.getId());
        return session;
    }

    @Override
    public boolean checkDataAccess(
            @Nullable final String login,
            @Nullable final String password
    ) {
        if (login.isEmpty() || password.isEmpty()) return false;
        final User user = serviceLocator.getUserService().findByLogin(login);
        if (user == null) return false;
        final String passwordHash = HashUtil.salt(serviceLocator.getPropertyService(), password);
        if (passwordHash == null || passwordHash.isEmpty()) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @SneakyThrows
    @Override
    public void validate(Session session) {
        if(session == null) throw new AccessDeniedException();
        if(session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if(session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        if(session.getTimestamp() == null) throw new AccessDeniedException();
        @NotNull final Session temp = session.clone();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if(!check) throw new AccessDeniedException();
        if(!sessionRepository.existsById(session.getId())) throw new AccessDeniedException();
    }

    @Override
    public void validate(Session session, Role role) {
        validate(session);
        @Nullable final User user = serviceLocator.getUserService().findById(session.getUserId());
        if(user == null) throw new AccessDeniedException();
        if(!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    public @Nullable Session sign(Session session) {
        if(session == null) return null;
        session.setSignature(null);
        @Nullable final String signature = HashUtil.sign(serviceLocator.getPropertyService(), session);
        session.setSignature(signature);
        return session;
    }

}
