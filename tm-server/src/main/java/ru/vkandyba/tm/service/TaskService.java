package ru.vkandyba.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.service.ITaskService;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.empty.EmptyIdException;
import ru.vkandyba.tm.exception.empty.EmptyIndexException;
import ru.vkandyba.tm.exception.empty.EmptyNameException;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.repository.AbstractBusinessRepository;

import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractBusinessService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    public Task findByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @NotNull
    @Override
    public Task removeByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    public Task updateByIndex(@Nullable String userId, @Nullable Integer index,
                              @Nullable String name, @Nullable String description)
    {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task updateById(@Nullable String userId, @Nullable String id,
                           @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findById(userId, id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void create(@Nullable String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
    }

    @Nullable
    @Override
    public Task startById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.startById(userId, id);
    }

    @Nullable
    @Override
    public Task startByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.startByIndex(userId, index);
    }

    @Nullable
    @Override
    public Task startByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.startByName(userId, name);
    }

    @Nullable
    @Override
    public Task finishById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return taskRepository.finishById(userId, id);
    }

    @Nullable
    @Override
    public Task finishByIndex(@Nullable String userId, @Nullable Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.finishByIndex(userId, index);
    }

    @Nullable
    @Override
    public Task finishByName(@Nullable String userId, @Nullable String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.finishByName(userId, name);
    }

    @Nullable
    @Override
    public Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new RuntimeException("Error! Status is empty");
        return taskRepository.finishById(userId, id);
    }

    @Nullable
    @Override
    public Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new RuntimeException("Error! Status is empty");
        return taskRepository.finishByIndex(userId, index);
    }

    @Nullable
    @Override
    public Task changeStatusByName(@Nullable String userId, @Nullable String name, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new RuntimeException("Error! Status is empty");
        return taskRepository.finishByName(userId, name);
    }

    @Override
    public void create(@Nullable String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new RuntimeException("Error! Description is empty");
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public void clear(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        taskRepository.clear(userId);
    }

}

