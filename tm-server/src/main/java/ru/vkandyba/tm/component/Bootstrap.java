package ru.vkandyba.tm.component;

import lombok.Builder;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.vkandyba.tm.api.endpoint.*;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.api.repository.ISessionRepository;
import ru.vkandyba.tm.api.repository.ITaskRepository;
import ru.vkandyba.tm.api.repository.IUserRepository;
import ru.vkandyba.tm.api.service.*;
import ru.vkandyba.tm.endpoint.*;
import ru.vkandyba.tm.enumerated.Role;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.repository.*;
import ru.vkandyba.tm.service.*;
import ru.vkandyba.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements ServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);
    @NotNull
    private final ILogService logService = new LogService();
    @NotNull
    private final IUserRepository userRepository = new UserRepository();
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);
    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this);
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();
    @NotNull
    private final ISessionService sessionService = new SessionService(this, sessionRepository);
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    private void initData() {
        projectService.add(new Project("Project Q", "-")).setStatus(Status.COMPLETED);
        projectService.add(new Project("Project B", "-"));
        projectService.add(new Project("Project F", "-")).setStatus(Status.IN_PROGRESS);
        projectService.add(new Project("Project D", "-")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Task A", "-")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Task D", "-")).setStatus(Status.COMPLETED);
        taskService.add(new Task("Task V", "-")).setStatus(Status.IN_PROGRESS);
        taskService.add(new Task("Task S", "-"));
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    @SneakyThrows
    private void initPID(){
        @NotNull final String fileName = "tsc-tm.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes() );
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }

    public void start(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        logService.debug("Test environment.");
        initData();
        initUsers();
        initPID();
        initEndpoint();
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(taskEndpoint);
        registry(projectEndpoint);
        registry(userEndpoint);
        registry(adminEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    @NotNull
    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @NotNull
    @Override
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @NotNull
    @Override
    public ISessionService getSessionService() {
        return sessionService;
    }
}
