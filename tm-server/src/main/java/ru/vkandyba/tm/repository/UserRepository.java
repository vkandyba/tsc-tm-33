package ru.vkandyba.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IUserRepository;
import ru.vkandyba.tm.exception.empty.EmptyLoginException;
import ru.vkandyba.tm.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findByLogin(@NotNull String login) {
        return list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @NotNull
    @Override
    public User removeUser(@NotNull User user) {
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull String login) {
        @Nullable final User user = findByLogin(login);
        if(user == null) throw new EmptyLoginException();
        list.remove(user);
        return user;
    }

}
