package ru.vkandyba.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vkandyba.tm.api.repository.IProjectRepository;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    @Nullable
    @Override
    public Project findByName(@NotNull String userId, @NotNull String name) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .filter(e -> name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Project removeByName(@NotNull String userId, @NotNull String name) {
        final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(this::remove);
        return project.orElse(null);
    }

    @Nullable
    @Override
    public Project startById(@NotNull String userId, @NotNull String id) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Nullable
    @Override
    public Project startByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Nullable
    @Override
    public Project startByName(@NotNull String userId, @NotNull String name) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Nullable
    @Override
    public Project finishById(@NotNull String userId, @NotNull String id) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Nullable
    @Override
    public Project finishByIndex(@NotNull String userId, @NotNull Integer index) {
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Nullable
    @Override
    public Project finishByName(@NotNull String userId, @NotNull String name) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status) {
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public void clear(@NotNull String userId) {
        list.removeAll(findAll(userId));
    }

}
